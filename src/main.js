import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import Datepicker from 'vuejs-datepicker'

import 'normalize.css'
import './assets/scss/main.scss'

Vue.prototype.$http = axios;

Vue.config.productionTip = false;

Vue.component('datepicker', Datepicker);

const token = localStorage.getItem('user-token');

if (token) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
