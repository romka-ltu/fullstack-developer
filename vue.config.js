const path = require("path");
const loader = {
    loader: 'sass-resources-loader',
    options: {
      resources: path.resolve(__dirname, './src/assets/scss/_settings.scss')
    }
}

// TODO: neveikia sass-resources-loader 

module.exports = {
    productionSourceMap: false,

    configureWebpack: {
        module: {
            rules: [
            {
                test: /\.scss$/,
                use: [
                loader,
                'sass-loader'
                ]
            }
            ]
        }
    }
};
